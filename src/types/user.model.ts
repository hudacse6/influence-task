import { CurrencyValue } from './money.model'

export interface UserToLogin {
  email: string
  password: string
  rememberMe: boolean
}

export interface UserResponseWithJWT {
  token?: string
  user: UserPublicInfo
}

export interface UserPublicInfo {
  id: string
  email: string
  money: Array<CurrencyValue>
  marketer: UserStatePublicInfo
  influencer: UserStatePublicInfo
  marketerInvoiceDetails?: InvoiceUserDetails
  influencerInvoiceDetails?: InvoiceUserDetails
  notificationsPreferences?: NotificationsPreferences
}

export interface UserStatePublicInfo {
  firstName: string
  lastName: string
  description: string
  avatarSimpleAccessFile: SimpleAccessFile
  rating: number
  moneyTransferred: number
  location?: string
  registerDate: string
}

export interface InvoiceUserDetails {
  name: string
  streetAddress: string
  city: string
  zipCode: string
  country: string
  vatId: string
}

export interface SimpleAccessFile {
  id: string
  filename: string
  size: number
  signedUrl: string
}

export interface NotificationsPreferences {
  chatWhenOffline: boolean
  platformWhenOffline: boolean
  platformWhenOnline: boolean
}
