import { computed, Ref, ref } from 'vue'
import { UserPublicInfo } from '@/types/user.model'

const myProfile: Ref<UserPublicInfo | null> = ref(null)

export default function useUserStore() {
  const getMyProfile = computed(() => {
    const user = myProfile.value
    if (user) {
      return user
    } else {
      const userState = localStorage.getItem('user')

      if (userState && typeof JSON.parse(userState) === 'object') return JSON.parse(userState)
      else return null
    }
  })
  const setMyProfile = (data: UserPublicInfo | null) => {
    myProfile.value = data
    if (data) {
      localStorage.setItem('user', JSON.stringify(data))
    }
  }

  const reset = () => {
    setMyProfile(null)
  }

  return {
    getMyProfile,
    setMyProfile,
    reset,
  }
}
